﻿using Common.Database;
using Common.Database.CustomDataModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Files
    {

        public static int addFile(int ExerciseId, string FileName, long FileSize,
            string FileType)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@ExerciseId", ExerciseId));
                pars.Add(new MySqlParameter("@FileName", FileName));
                pars.Add(new MySqlParameter("@FileSize", FileSize));
                pars.Add(new MySqlParameter("@FileType", FileType));

                string sql = @"insert into files
                                (guid,name,size,type,exerciseId)
                                values
                                (@Guid,@FileName,@FileSize,@FileType,@ExerciseId);
                                SELECT LAST_INSERT_ID() as id;";

                int FileId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return FileId;
            }
        }

        public static void updateFile(int ExerciseId, string FileName, long FileSize,
            string FileType)
        {
            using (var context = new gymentor_dataEntities())
            {

                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@ExerciseId", ExerciseId));
                
                //check if there is a file for this exercise..
                string sql = @"SELECT * FROM files where exerciseId = @ExerciseId;";

                file File = context.Database.SqlQuery<file>(sql, pars.ToArray()).FirstOrDefault();

                if (File==null)
                {
                    addFile(ExerciseId, FileName, FileSize, FileType);
                    return;
                }

                pars.Add(new MySqlParameter("@FileName", FileName));
                pars.Add(new MySqlParameter("@FileSize", FileSize));
                pars.Add(new MySqlParameter("@FileType", FileType));

                sql = @"update files set
                                name = @FileName,
                                size = @FileSize,
                                type = @FileType
                                where exerciseId = @ExerciseId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }

        public static int addProfilePic(int UserId, string FileName, long FileSize,
            string FileType)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@UserId", UserId));
                pars.Add(new MySqlParameter("@FileName", FileName));
                pars.Add(new MySqlParameter("@FileSize", FileSize));
                pars.Add(new MySqlParameter("@FileType", FileType));

                string sql = @"insert into profile_pictures
                                (guid,name,size,type,userId)
                                values
                                (@Guid,@FileName,@FileSize,@FileType,@UserId);
                                SELECT LAST_INSERT_ID() as id;";

                int FileId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return FileId;
            }
        }

        public static void updateProfilePic(int UserId, string FileName, long FileSize,
            string FileType)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@UserId", UserId));
                pars.Add(new MySqlParameter("@FileName", FileName));
                pars.Add(new MySqlParameter("@FileSize", FileSize));
                pars.Add(new MySqlParameter("@FileType", FileType));

                string sql = @"update profile_pictures set
                                name = @FileName,
                                size = @FileSize,
                                type = @FileType
                                where userId = @UserId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }

        //public static file getFile(string Guid)
        //{
        //    List<MySqlParameter> pars = new List<MySqlParameter>();
        //    pars.Add(new MySqlParameter("@Guid", Guid));
        //    string sql = "select * from files where deleted = false and Guid = @Guid";
        //    using (var context = new netgenEntities())
        //    {
        //        file file = context.Database.SqlQuery<file>(sql, pars.ToArray()).FirstOrDefault();
        //        return file;
        //    }
        //}

        public static file getFileForExercise(int ExerciseId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@ExerciseId", ExerciseId));
            string sql = "select * from files where deleted = false and exerciseId = @ExerciseId";
            using (var context = new gymentor_dataEntities())
            {
                file file = context.Database.SqlQuery<file>(sql, pars.ToArray()).FirstOrDefault();
                return file;
            }
        }

        public static profile_pictures geProfilePicture(int UserId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@UserId", UserId));
            string sql = "select * from profile_pictures where deleted = false and userId = @UserId";
            using (var context = new gymentor_dataEntities())
            {
                profile_pictures file = context.Database.SqlQuery<profile_pictures>(sql, pars.ToArray()).FirstOrDefault();
                return file;
            }
        }


    }
}
