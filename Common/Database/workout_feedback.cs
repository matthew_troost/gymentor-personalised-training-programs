//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Common.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class workout_feedback
    {
        public int id { get; set; }
        public Nullable<int> userId { get; set; }
        public Nullable<int> programId { get; set; }
        public Nullable<System.DateTime> last_updated { get; set; }
        public Nullable<bool> deleted { get; set; }
        public string feedback { get; set; }
    }
}
