//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Common.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class discount_code
    {
        public int id { get; set; }
        public Nullable<int> percentage { get; set; }
        public Nullable<bool> active { get; set; }
        public Nullable<bool> deleted { get; set; }
        public string code { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
    }
}
