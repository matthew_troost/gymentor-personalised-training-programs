//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Common.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class workout_element
    {
        public int id { get; set; }
        public Nullable<int> exerciseId { get; set; }
        public string repcount { get; set; }
        public Nullable<int> sets { get; set; }
        public string rest { get; set; }
        public string comment { get; set; }
        public Nullable<bool> deleted { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<int> programId { get; set; }
        public Nullable<int> dayNumber { get; set; }
    }
}
