﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.CustomDataModels
{
    public class current_program_stats_custom
    {
        public int id { get; set; }
        public int clientId { get; set; }
        public int program_days { get; set; }
        public int completed_days { get; set; }
        public int programId { get; set; }
        public Nullable<bool> deleted { get; set; }
        public string checked_days { get; set; }
        public string name { get; set; }
        public DateTime date_assigned { get; set; }
        public DateTime start_date { get; set; }
    }
}
