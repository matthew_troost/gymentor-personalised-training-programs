﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.CustomDataModels
{
    public class chatroom_userInfo_custom
    {
        public int id { get; set; }
        public Nullable<bool> unread_by_mentor { get; set; }
        public Nullable<bool> unread_by_user { get; set; }
        public Nullable<int> userId { get; set; }
        public Nullable<int> number_of_messages { get; set; }
        public DateTime date_created { get; set; }
        public Nullable<int> last_message_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string message { get; set; }
    }
}
