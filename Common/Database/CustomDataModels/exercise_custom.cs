﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.CustomDataModels
{
    public class exercise_custom
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Nullable<int> levelId { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<bool> deleted { get; set; }
        public string guid { get; set; }
        public string levelName { get; set; }
        
    }
}
