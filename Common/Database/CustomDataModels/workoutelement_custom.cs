﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.CustomDataModels
{
    public class workoutelement_custom
    {
        public int id { get; set; }
        public Nullable<int> exerciseId { get; set; }
        public string exerciseName { get; set; }
        public Nullable<int> fileId { get; set; }
        public string repcount { get; set; }
        public Nullable<int> sets { get; set; }
        public string rest { get; set; }
        public string comment { get; set; }
        public string description { get; set; }
        public Nullable<bool> deleted { get; set; }
        public DateTime date_created { get; set; }
        public string filename { get; set; }

    }
}
