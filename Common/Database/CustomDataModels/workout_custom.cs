﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Database.CustomDataModels
{
    public class workout_custom
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<int> clientId { get; set; }
        public System.DateTime date_created { get; set; }
        public Nullable<bool> deleted { get; set; }
        public Nullable<bool> completed { get; set; }
        public Nullable<int> number_of_days { get; set; }
        public string guid { get; set; }
        public DateTime start_date { get; set; }
        public DateTime date_assigned { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public Nullable<int> completed_days { get; set; }


    }
}
