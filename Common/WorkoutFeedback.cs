﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class WorkoutFeedback
    {
        public static IEnumerable<workout_feedback> getFeedback()
        {
            string sql = @"select * from workout_feedback where deleted = 0 order by last_updated desc;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<workout_feedback> feedback = context.Database.SqlQuery<workout_feedback>(sql).ToList();
                return feedback;
            }
        }

        public static IEnumerable<workout_feedback> getLatestFeedback()
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@date", DateTime.Parse(DateTime.Today.AddDays(-5).ToString("yyyy-MM-dd"))));

            string sql = @"select * from workout_feedback 
	                        where deleted = 0 
                            and last_updated > @date
	                        order by last_updated desc;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<workout_feedback> feedback = context.Database.SqlQuery<workout_feedback>(sql,pars.ToArray()).ToList();
                return feedback;
            }
        }

        public static workout_feedback getFeedback(int userId, int programId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@userId", userId));
            pars.Add(new MySqlParameter("@programId", programId));

            string sql = @"select * from workout_feedback 
                            where deleted = 0
                            and userId = @userId
                            and programId = @programId;";

            using (var context = new gymentor_dataEntities())
            {
                workout_feedback feedback = context.Database.SqlQuery<workout_feedback>(sql,pars.ToArray()).FirstOrDefault();
                return feedback;
            }
        }

        public static int addFeedback(string feedback, int userId, int programId)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@feedback", feedback));
                pars.Add(new MySqlParameter("@programId", programId));
                pars.Add(new MySqlParameter("@userId", userId));
                pars.Add(new MySqlParameter("@date", DateTime.Parse(DateTime.Today.ToString("yyyy-MM-dd"))));

                string sql = @"insert into workout_feedback
                                (userId,programId,last_updated,feedback)
                                values
                                (@userId,@programId,@date,@feedback);
                                SELECT LAST_INSERT_ID() as id;";

                int feedbackId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return feedbackId;
            }
        }

        public static void updateFeedback(int id, string feedback)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@feedback", feedback));
                pars.Add(new MySqlParameter("@date", DateTime.Parse(DateTime.Today.ToString("yyyy-MM-dd"))));

                string sql = @"update workout_feedback set
                                feedback = @feedback,
                                last_updated = @date
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static bool deleteFeedback(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update workout_feedback set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static bool deleteAllFeedbackForProgram(int programId)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@programId", programId));

                string sql = @"update workout_feedback set
                                deleted = true
                                where programId = @programId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

    }
}
