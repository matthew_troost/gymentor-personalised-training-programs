﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class User
    {

        public static IEnumerable<user> getUsers()
        {
            string sql = @"select * from users where deleted=0 and superUser = 0";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<user> users = context.Database.SqlQuery<user>(sql).ToList();
                return users;
            }
        }

        public static IEnumerable<user> getLegitUsers()
        {
            string sql = @"select u.* from users u
	                        inner join user_stats s on s.userId = u.id
	                        where u.deleted=0 
	                        and u.superUser = 0";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<user> users = context.Database.SqlQuery<user>(sql).ToList();
                return users;
            }
        }

        public static IEnumerable<user> getMentors()
        {
            string sql = @"select * from users where deleted=0 and superUser = 1";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<user> users = context.Database.SqlQuery<user>(sql).ToList();
                return users;
            }
        }

        public static user CheckUserNameAvailability(string username, string firstname, string lastname)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@username", username));
            pars.Add(new MySqlParameter("@firstname", firstname));
            pars.Add(new MySqlParameter("@lastname", lastname));

            string sql = @"select * from users where username=@username or (firstname =@firstname and lastname = @lastname)";

            using (var context = new gymentor_dataEntities())
            {
                user user = context.Database.SqlQuery<user>(sql,pars.ToArray()).FirstOrDefault();
                return user;
            }
        }

        public static IEnumerable<user> getChatUsers()
        {
            string sql = @"select * from users 
                            where deleted=0
                            and superUser = 0
                            and chatRoom_open = 1";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<user> users = context.Database.SqlQuery<user>(sql).ToList();
                return users;
            }
        }

        public static user getUser(string id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select * from users 
                            where deleted=0
                            and guid = @id;";

            using (var context = new gymentor_dataEntities())
            {
                user user = context.Database.SqlQuery<user>(sql,pars.ToArray()).FirstOrDefault();
                return user;
            }
        }

        public static user getUserbyUsername(string username)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@username", username));

            string sql = @"select * from users where (username = @username or email = @username);";

            using (var context = new gymentor_dataEntities())
            {
                user user = context.Database.SqlQuery<user>(sql, pars.ToArray()).FirstOrDefault();
                if (user == null)
                    return new user();

                return user;
            }
        }


        public static void updateLastSeen(int userID)
        {
            using (var context = new gymentor_dataEntities())
            {
                DateTime now = DateTime.Today;

                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", userID));
                pars.Add(new MySqlParameter("@now", now));

                string sql = @"update users set
                                last_seen = @now
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static int getNumberOfUsers()
        {
            string sql = @"SELECT count(id) from users where deleted=0 and superUser=0;";

            using (var context = new gymentor_dataEntities())
            {
                int users = context.Database.SqlQuery<int>(sql).FirstOrDefault();
                return users;
            }
        }

        public static int getNewUsers()
        {
            string DaysAgo = DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd") + " 00:00:00";

            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@DaysAgo", DaysAgo));

            string sql = @"SELECT count(id) from users where deleted=0 and date_joined > @DaysAgo;";

            using (var context = new gymentor_dataEntities())
            {
                int users = context.Database.SqlQuery<int>(sql,pars.ToArray()).FirstOrDefault();
                return users;
            }
        }

        public static user getUser(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select * from users 
                            where deleted=0
                            and id = @id;";

            using (var context = new gymentor_dataEntities())
            {
                user user = context.Database.SqlQuery<user>(sql, pars.ToArray()).FirstOrDefault();
                return user;
            }
        }

        public static int addUser(string firstname, string lastname, string email, string password, string username, string secQ, string secA)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@firstname", firstname));
                pars.Add(new MySqlParameter("@lastname", lastname));
                pars.Add(new MySqlParameter("@email", email));
                pars.Add(new MySqlParameter("@password", password));
                pars.Add(new MySqlParameter("@username", username));
                pars.Add(new MySqlParameter("@secQ", secQ));
                pars.Add(new MySqlParameter("@secA", secA));

                string sql = @"insert into users
                                (guid,firstname,lastname,email,password,username,security_question,security_answer)
                                values
                                (@Guid,@firstname,@lastname,@email,@password,@username,@secQ,@secA);
                                SELECT LAST_INSERT_ID() as id;";

                int userId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return userId;
            }
        }

        public static int addMentor(string firstname, string lastname, string email, string username)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@firstname", firstname));
                pars.Add(new MySqlParameter("@lastname", lastname));
                pars.Add(new MySqlParameter("@email", email));
                pars.Add(new MySqlParameter("@username", username));


                string sql = @"insert into users
                                (guid,firstname,lastname,email,password,username,superUser)
                                values
                                (@Guid,@firstname,@lastname,@email,'admin',@username,'1');
                                SELECT LAST_INSERT_ID() as id;";

                int userId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return userId;
            }
        }

        public static user login(string Username, string Password)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@Username", Username));
            pars.Add(new MySqlParameter("@Password", Password));

            string sql = "select * from users where (username = @Username or email = @Username) and password = @Password";

            using (var context = new gymentor_dataEntities())
            {
                user user = context.Database.SqlQuery<user>(sql, pars.ToArray()).FirstOrDefault();
                return user;
            }
        }


    }
}
