﻿using Common.Database.CustomDataModels;
using Common.Database;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public class ChatRoomMessage
    {
        public static IEnumerable<chatroom_messages> getMessages(int UserId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", UserId));

            string sql = @"select * 
	                        from chatroom_messages
                            where deleted = 0
                            and userId = @id";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<chatroom_messages> messages = context.Database.SqlQuery<chatroom_messages>(sql, pars.ToArray()).ToList();
                return messages;
            }
        }

        public static chatroom_userInfo_custom getChatRoomUserInfo(int UserId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", UserId));

            string sql = @"select ci.*, u.firstname, u.lastname, cm.message, cm.date_created  
	                        from chatroom_user_info ci
                            inner join users u on ci.userId = u.id
                            inner join chatroom_messages cm on ci.last_message_id = cm.id
                            where ci.userId = @id";

            using (var context = new gymentor_dataEntities())
            {
                chatroom_userInfo_custom info = context.Database.SqlQuery<chatroom_userInfo_custom>(sql, pars.ToArray()).FirstOrDefault();
                return info;
            }
        }

        public static bool updateUnreadByUser(int UserId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", UserId));

            string sql = @"update chatroom_user_info set
                                unread_by_user = false
                                where userId = @id";

            using (var context = new gymentor_dataEntities())
            {
                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static int getNewMessagesForMentor()
        {
            string sql = @"select count(id) from chatroom_user_info
                            where unread_by_mentor = true";

            using (var context = new gymentor_dataEntities())
            {
                int messages = context.Database.SqlQuery<int>(sql).FirstOrDefault();
                return messages;
            }
        }

        public static int sendMessage(int userId, string html, bool fromMentor, bool readbyMentor)
         {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@userId", userId));
                pars.Add(new MySqlParameter("@html", html));
                pars.Add(new MySqlParameter("@fromMentor", fromMentor));
                pars.Add(new MySqlParameter("@readbyMentor", readbyMentor));

                string sql = @"insert into chatroom_messages
                                (userId,message,readByMentor,fromMentor)
                                values
                                (@userId,@html,@fromMentor,@readbyMentor);
                            SELECT max(id) from chatroom_messages;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();



                //check if user has user info i.e has sent mail before
                pars.Clear();
                pars.Add(new MySqlParameter("@userId", userId));

                sql = @"select * from chatroom_user_info where userId = @userId;";

                chatroom_user_info info = context.Database.SqlQuery<chatroom_user_info>(sql, pars.ToArray()).FirstOrDefault();

                if (info == null)
                {

                    //add new messega info for this client 
                    pars.Add(new MySqlParameter("@id", id));

                    sql = @"insert into chatroom_user_info 
                                (userId, number_of_messages, last_message_id, unread_by_user, unread_by_mentor)
                                values ";

                    if (fromMentor == true)
                        sql += "(@userId, 1, @id, true, false); ";
                    else
                        sql += "(@userId, 1, @id, false, true); ";


                   sql += @"update users set
                                chatRoom_open = true
                                where id = @userId;";

                    context.Database.ExecuteSqlCommand(sql, pars.ToArray());

                }
                else { 

                    //update user message info 
                    pars.Add(new MySqlParameter("@id", id));

                    sql = @"update chatroom_user_info set
                                    number_of_messages = number_of_messages + 1,
                                    last_message_id = @id, ";

                    if (fromMentor == true)
                        sql += "unread_by_user = true, unread_by_mentor = false where userId = @userId;";
                    else
                        sql += "unread_by_user = false, unread_by_mentor = true where userId = @userId;";

                    context.Database.ExecuteSqlCommand(sql, pars.ToArray());

                }

                return id;
            }
        }

    }
}
