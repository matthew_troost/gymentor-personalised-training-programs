﻿using Common.Database;
using Common.Database.CustomDataModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class WorkoutElement
    {

        public static IEnumerable<workoutelement_custom> getWorkoutElements()
        {
            string sql = @"SELECT w.*, e.`name` as exerciseName, f.id as fileId, e.description, f.name as filename
	                        from workout_element w
                            left join exercise e on e.id = w.exerciseId
                            left join files f on f.exerciseId = w.exerciseId
                            where w.deleted = 0;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<workoutelement_custom> elements = context.Database.SqlQuery<workoutelement_custom>(sql).ToList();
                return elements;
            }
        }


        public static int addWorkoutElement(int exerciseId, string reps, int sets, string rest, string comment, int programId, int day)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@exerciseId", exerciseId));
                pars.Add(new MySqlParameter("@reps", reps));
                pars.Add(new MySqlParameter("@sets", sets));
                pars.Add(new MySqlParameter("@rest", rest));
                pars.Add(new MySqlParameter("@comment", comment));
                pars.Add(new MySqlParameter("@programId", programId));
                pars.Add(new MySqlParameter("@day", day));

                string sql = @"insert into workout_element
                                (exerciseId,repcount,sets,rest,comment,programId,dayNumber)
                                values
                                (@exerciseId,@reps,@sets,@rest,@comment,@programId,@day);
                                SELECT LAST_INSERT_ID() as id;";

                int workoutElementId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return workoutElementId;
            }
        }


        public static void deleteWorkoutElement(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update workout_element set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static void updateWorkoutElement(int id, int exerciseId, string reps, int sets, string rest, string comment)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@exerciseId", exerciseId));
                pars.Add(new MySqlParameter("@reps", reps));
                pars.Add(new MySqlParameter("@sets", sets));
                pars.Add(new MySqlParameter("@rest", rest));
                pars.Add(new MySqlParameter("@comment", comment));

                string sql = @"update workout_element set
                                exerciseId = @exerciseId,
                                repcount = @reps,
                                sets = @sets,
                                rest = @rest,
                                comment = @comment
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }




        public static IEnumerable<workoutelement_custom> getWorkoutElementsPerDay(int programId, int day)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@programId", programId));
            pars.Add(new MySqlParameter("@day", day));

            string sql = @"SELECT w.*, e.`name` as exerciseName, f.id as fileId, e.description, f.name as filename
	                        from workout_element w
	                        left join exercise e on e.id = w.exerciseId
	                        left join files f on f.exerciseId = w.exerciseId
	                        where w.deleted = 0
                            and w.programId = @programId
                            and w.dayNumber = @day
                            order by w.id;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<workoutelement_custom> elements = context.Database.SqlQuery<workoutelement_custom>(sql, pars.ToArray()).ToArray();
                return elements;
            }
        }

    }
}
