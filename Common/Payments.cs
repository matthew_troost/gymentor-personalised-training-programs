﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Common;
using System.Web;
using System.Collections.Specialized;
using System.Collections;

namespace Common
{
    public class Payments
    {

        public static string btnOrder_Click(string amount, int userId)
        {
            try
            {
                // Create the order in your DB and get the ID
                int orderId = Orders.addOrder(amount, userId);
                //get the order details
                order order = Orders.getOrder(orderId);

                //add method in Payments for the notify url where we will set the oder success to true
                string name = "Gymentor Membership, Order #" + orderId.ToString();
                string description = "Payment";

                string site = "https://www.payfast.co.za/eng/process?";
                string merchant_id = "11949075";
                string merchant_key = "7y49wq9emodlv";

                string returnURL = "http://tracker.gymentor.co.za/Payments/Pay";
                string cancelURL = "http://tracker.gymentor.co.za/Payments/Pay";
                string notifyURL = "http://tracker.gymentor.co.za/Payments/PaymentSuccess/" + order.orderId;


                // Build the query string for payment site

                StringBuilder str = new StringBuilder();
                str.Append("merchant_id=" + HttpUtility.UrlEncode(merchant_id));
                str.Append("&merchant_key=" + HttpUtility.UrlEncode(merchant_key));
                str.Append("&return_url=" + HttpUtility.UrlEncode(returnURL));
                str.Append("&cancel_url=" + HttpUtility.UrlEncode(cancelURL));
                str.Append("&notify_url=" + HttpUtility.UrlEncode(notifyURL));

                str.Append("&m_payment_id=" + HttpUtility.UrlEncode(orderId.ToString()));
                str.Append("&amount=" + HttpUtility.UrlEncode(amount));
                str.Append("&item_name=" + HttpUtility.UrlEncode(name));
                str.Append("&item_description=" + HttpUtility.UrlEncode(description));

                // Redirect to PayFast
                return (site + str.ToString());
            }
            catch (Exception ex)
            {
                return ex.ToString();
                // Handle your errors here (log them and tell the user that there was an error)
            }
        }


        public static void PerformSecurityChecks(NameValueCollection arrPostedVariables, string merchant_id)
        {
            // Verify that we are the intended merchant
            string receivedMerchant = arrPostedVariables["merchant_id"];

            if (receivedMerchant != merchant_id)
                throw new Exception("Mechant ID mismatch");

            // Verify that the request comes from the payment processor's servers.

            // Get all valid websites from payment processor
            string[] validSites = new string[] { "www.payfast.co.za", "sandbox.payfast.co.za", "w1w.payfast.co.za", "w2w.payfast.co.za" };

            // Get the requesting ip address
            string requestIp = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(requestIp))
                throw new Exception("IP address cannot be null");

            // Is address in list of websites
            if (!IsIpAddressInList(validSites, requestIp))
                throw new Exception("IP address invalid");
        }

        public static bool IsIpAddressInList(string[] validSites, string ipAddress)
        {
            // Get the ip addresses of the websites
            ArrayList validIps = new ArrayList();

            for (int i = 0; i < validSites.Length; i++)
            {
                validIps.AddRange(System.Net.Dns.GetHostAddresses(validSites[i]));
            }

            IPAddress ipObject = IPAddress.Parse(ipAddress);

            if (validIps.Contains(ipObject))
                return true;
            else
                return false;
        }

        public static void ValidateData(string site, NameValueCollection arrPostedVariables)
        {
            WebClient webClient = null;

            try
            {
                webClient = new WebClient();
                byte[] responseArray = webClient.UploadValues(site, arrPostedVariables);

                // Get the response and replace the line breaks with spaces
                string result = Encoding.ASCII.GetString(responseArray);
                result = result.Replace("\r\n", " ").Replace("\r", "").Replace("\n", " ");

                // Was the data valid?
                if (result == null || !result.StartsWith("VALID"))
                    throw new Exception("Data validation failed");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (webClient != null)
                    webClient.Dispose();
            }
        }

        public static void ProcessOrder(NameValueCollection arrPostedVariables, string OrderId)
        {
            // Determine from payment status if we are supposed to credit or not
            string paymentStatus = arrPostedVariables["payment_status"];

            try
            {
                if (paymentStatus == "COMPLETE")
                {
                    // Update order to successful
                    Orders.orderSuccess(OrderId);
                }
            }
            catch (Exception ex)
            {
                // Handle errors here
            }
        }







        public static IEnumerable<discount_code> getDiscountCodes()
        {
            string sql = @"select *
                            from discount_code
                            where deleted = 0
                            order by active desc, percentage asc;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<discount_code> codes = context.Database.SqlQuery<discount_code>(sql).ToList();
                return codes;
            }
        }

        public static discount_code confirmDiscountCode(string code)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@code", code));

                string sql = @"select *
                            from discount_code
                            where deleted = 0
                            and active = 1
                            and code = @code;";

                discount_code discount = context.Database.SqlQuery<discount_code>(sql, pars.ToArray()).FirstOrDefault();

                if (discount != null)
                    deactivateDiscountCode(discount.id);

                return discount;
            }
        }

        public static int addDiscountCode(int discountpercentage)
        {
            using (var context = new gymentor_dataEntities())
            {
                string code = Guid.NewGuid().ToString("N").Substring(0,5);

                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@discount", discountpercentage));
                pars.Add(new MySqlParameter("@code", code));

                string sql = @"insert into discount_code
                                (percentage,code)
                                values
                                (@discount,@code);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return id;
            }
        }

        public static bool deleteDiscountCode(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update discount_code set
                                deleted = 1,
                                active = 0
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
            return true;
        }


        public static bool activateDiscountCode(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update discount_code set
                                active = 1
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
            return true;
        }


        public static bool deactivateDiscountCode(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update discount_code set
                                active = 0
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
            return true;
        }

    }
}
