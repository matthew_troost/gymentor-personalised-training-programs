﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Membership
    {

        public static IEnumerable<membership> getMemberships()
        {
            string sql = @"select *
                            from memberships 
                            where deleted=0";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<membership> memberships = context.Database.SqlQuery<membership>(sql).ToList();
                return memberships;
            }
        }

        public static membership getMembership(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select *
                            from memberships 
                            where deleted=0
                            and id = @id;";

            using (var context = new gymentor_dataEntities())
            {
                membership membership = context.Database.SqlQuery<membership>(sql, pars.ToArray()).FirstOrDefault();
                return membership;
            }
        }

        public static int addMembership(string name, int price, int months)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@price", price));
                pars.Add(new MySqlParameter("@months", months));

                string sql = @"insert into memberships
                                (name,price,number_of_months)
                                values
                                (@name,@price,@months);
                                SELECT LAST_INSERT_ID() as id;";

                int MembershipId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return MembershipId;
            }
        }

        public static void updateMembership(int id, string name, int price, int months)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@price", price));
                pars.Add(new MySqlParameter("@months", months));

                string sql = @"update memberships set
                                name = @name,
                                price = @price,
                                number_of_months = @months
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static bool deleteExercise(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update memberships set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }




    }
}
