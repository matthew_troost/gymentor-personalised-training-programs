﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Orders
    {
        public static int addOrder(string amount, int userId)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@amount", amount));
                pars.Add(new MySqlParameter("@userId", userId));

                string sql = @"insert into orders
                                (amount,orderId,clientId)
                                values
                                (@amount,@Guid,@userId);
                                SELECT LAST_INSERT_ID() as id;";

                int orederId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return orederId;
            }
        }

        public static void orderSuccess(string id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update orders set
                                successfull = 1
                                where orderId = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }

        public static IEnumerable<order> getOrders()
        {
            string sql = @"select o.* from orders o
	                        inner join users u on u.id = o.clientId
	                        where u.deleted = 0
                            order by date desc;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<order> orders = context.Database.SqlQuery<order>(sql).ToList();
                return orders;
            }
        }

        public static IEnumerable<order> getLatestOrders()
        {
            string DaysAgo = DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd") + " 00:00:00";

            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@DaysAgo", DaysAgo));

            string sql = @"select o.* from orders o
	                        inner join users u on u.id = o.clientId
	                        where u.deleted = 0
                            and o.date > @DaysAgo
                            order by date desc;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<order> orders = context.Database.SqlQuery<order>(sql, pars.ToArray()).ToList();
                return orders;
            }
        }

        public static order getOrder(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select * from orders where id=@id;";

            using (var context = new gymentor_dataEntities())
            {
                order order = context.Database.SqlQuery<order>(sql,pars.ToArray()).FirstOrDefault();
                return order;
            }
        }

        public static order getOrderByOrderID(string id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select * from orders where orderId=@id;";

            using (var context = new gymentor_dataEntities())
            {
                order order = context.Database.SqlQuery<order>(sql, pars.ToArray()).FirstOrDefault();
                return order;
            }
        }

        public static int getLastOrderNumber()
        {
            string sql = @"select MAX(id) from orders;";

            using (var context = new gymentor_dataEntities())
            {
                int order = context.Database.SqlQuery<int>(sql).FirstOrDefault();
                return order;
            }
        }

        public static IEnumerable<order> getOrdersForUser(int userId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@userId", userId));

            string sql = @"select * from orders where clientId = @userId;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<order> orders = context.Database.SqlQuery<order>(sql,pars.ToArray()).ToList();
                return orders;
            }
        }

    }
}
