﻿using Common.Database;
using Common.Database.CustomDataModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Workout
    {

        public static IEnumerable<workout_custom> getWorkouts()
        {
            string sql = @"select w.*, u.firstname, u.lastname, c.completed_days
                            from workout w
                            inner join users u on w.clientId = u.id
                            left join client_current_program c on w.id = c.programId
                            where w.deleted = 0
                            and c.deleted = 0
                            order by date_assigned desc;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<workout_custom> workouts = context.Database.SqlQuery<workout_custom>(sql).ToList();
                return workouts;
            }
        }

        public static int getNumberOfWorkouts()
        {
            return getWorkouts().Count();
        }

        public static workout_custom getWorkout(string guid)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@guid", guid));

            string sql = @"select w.*, u.firstname, u.lastname
                            from workout w
                            inner join users u on w.clientId = u.id
                            where w.deleted = 0
                            and w.guid = @guid;";

            using (var context = new gymentor_dataEntities())
            {
                workout_custom workout = context.Database.SqlQuery<workout_custom>(sql, pars.ToArray()).FirstOrDefault();
                return workout;
            }
        }

        public static workout_custom getWorkout(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select w.*, u.firstname, u.lastname
                            from workout w
                            inner join users u on w.clientId = u.id
                            where w.deleted = 0
                            and w.id = @id;";

            using (var context = new gymentor_dataEntities())
            {
                workout_custom workout = context.Database.SqlQuery<workout_custom>(sql, pars.ToArray()).FirstOrDefault();
                return workout;
            }
        }

        public static IEnumerable<workout_custom> getWorkoutsForUser(int UserId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@UserId", UserId));

            string now = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            pars.Add(new MySqlParameter("@dateAssigned", now));

            string sql = @"select w.*, u.firstname, u.lastname
                            from workout w
                            inner join users u on w.clientId = u.id
                            where w.deleted = 0
                            and w.completed = 0
                            and w.clientId = @UserId
                            and w.date_assigned > @dateAssigned
                            and start_date <= @dateAssigned;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<workout_custom> workouts = context.Database.SqlQuery<workout_custom>(sql, pars.ToArray()).ToList();
                return workouts;
            }
        }

        public static client_current_program getClientProgramDetails(int UserId, int ProgramId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@UserId", UserId));
            pars.Add(new MySqlParameter("@ProgramId", ProgramId));

            string sql = @"select *
                            from client_current_program
                            where deleted = 0
                            and clientId = @UserId
                            and programId = @ProgramId;";

            using (var context = new gymentor_dataEntities())
            {
                client_current_program details = context.Database.SqlQuery<client_current_program>(sql, pars.ToArray()).FirstOrDefault();
                return details;
            }
        }

        public static bool deleteClientProgramDetails(int programID)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@programID", programID));

                string sql = @"update client_current_program set
                                deleted = true
                                where programId = @programID;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static bool deleteAllUserClientProgramDetails(int userId)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@userId", userId));

                string sql = @"update client_current_program set
                                deleted = true
                                where clientId = @userId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static int getSpecificAllClientProgramDetails(int UserId, int ProgramId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@UserId", UserId));
            pars.Add(new MySqlParameter("@ProgramId", ProgramId));

            string sql = @"select *
                            from client_current_program
                            where deleted = 0
                            and clientId != @UserId
                            and programId = @ProgramId
                            order by programId;";

            using (var context = new gymentor_dataEntities())
            {
                client_current_program details = context.Database.SqlQuery<client_current_program>(sql, pars.ToArray()).FirstOrDefault();
                if (details == null)
                    return 0;

                return details.id;
            }
        }

        public static IEnumerable<client_current_program> getAllClientProgramDetails(int UserId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@UserId", UserId));

            string sql = @"select c.*
                            from client_current_program c
                            inner join workout w on c.programId = w.id
                            where c.deleted = 0
                            and w.deleted = 0
                            and c.clientId = @UserId
                            order by c.programId;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<client_current_program> details = context.Database.SqlQuery<client_current_program>(sql, pars.ToArray()).ToList();
                return details;
            }
        }

        public static int getAllActivePrograms()
        {
            string today = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";

            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@today", today));

            string sql = @"SELECT count(id) from workout where deleted=0 and start_date <= @today and date_assigned >= @today;";

            using (var context = new gymentor_dataEntities())
            {
                int number = context.Database.SqlQuery<int>(sql,pars.ToArray()).FirstOrDefault();
                return number;
            }
        }

        public static bool isClientActive(int userId)
        {
            string today = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";

            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@today", today));
            pars.Add(new MySqlParameter("@userId", userId));

            string sql = @"SELECT count(id) from workout where deleted=0 and clientId = @userId and start_date <= @today and date_assigned >= @today;";

            using (var context = new gymentor_dataEntities())
            {
                int number = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                if(number == 0)
                    return false;
                else
                    return true;
            }
        }

        public static current_program_stats_custom getCustomClientProgramDetails(int UserId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@UserId", UserId));
            pars.Add(new MySqlParameter("@today", DateTime.Parse(DateTime.Today.ToString("yyyy-MM-dd"))));

            string sql = @"SELECT c.*, `name`, date_assigned, start_date 
                            FROM client_current_program c
                            inner join workout w on c.programId = w.id
                            where c.clientId = @UserId
                            and c.deleted = 0
                            and w.deleted = 0
                            and start_date <= @today
                            and date_assigned > @today;";

            using (var context = new gymentor_dataEntities())
            {
                current_program_stats_custom details = context.Database.SqlQuery<current_program_stats_custom>(sql, pars.ToArray()).FirstOrDefault();
                return details;
            }
        }

        public static int addClientProgramDetails(int UserId, int programdays, int programid)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@UserId", UserId));
                pars.Add(new MySqlParameter("@programdays", programdays));
                pars.Add(new MySqlParameter("@programid", programid));

                string sql = @"insert into client_current_program
                                (clientId,program_days,programId)
                                values
                                (@UserId,@programdays,@programid);
                                SELECT LAST_INSERT_ID() as id;";

                int id = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return id;
            }
        }


        public static void updateClientProgramDetailsMentor(int UserId, int programdays, int programid)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@UserId", UserId));
                pars.Add(new MySqlParameter("@programdays", programdays));
                pars.Add(new MySqlParameter("@programid", programid));

                string sql = @"update client_current_program set
                                program_days = @programdays
                                where clientId = @UserId
                                and programId = @programid;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static bool updateClientProgramDetailsUser(int UserId, int completedDays, string checkedDays, int programId)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@UserId", UserId));
                pars.Add(new MySqlParameter("@completedDays", completedDays));
                pars.Add(new MySqlParameter("@checkedDays", checkedDays));
                pars.Add(new MySqlParameter("@programId", programId));

                string sql = @"update client_current_program set
                                completed_days = @completedDays,
                                checked_days = @checkedDays
                                where clientId = @UserId
                                and programId = @programId
                                and deleted = 0;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

        public static int addWorkout(string name, int clientId)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@clientId", clientId));

                string sql = @"insert into workout
                                (guid,name,clientId)
                                values
                                (@Guid,@name,@clientId);
                                SELECT LAST_INSERT_ID() as id;";

                int workoutId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return workoutId;
            }
        }


        public static void updateWorkout(int id, string name, int clientId, int number_of_days, DateTime start_date, DateTime expiryDate)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@clientId", clientId));
                pars.Add(new MySqlParameter("@number_of_days", number_of_days));
                pars.Add(new MySqlParameter("@expiryDate", expiryDate));
                pars.Add(new MySqlParameter("@start_date", start_date));

                string sql = @"update workout set
                                name = @name,
                                clientId = @clientId,
                                number_of_days = @number_of_days,
                                start_date = @start_date,
                                date_assigned = @expiryDate
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static string copyWorkout(int workoutId)
        {
            using (var context = new gymentor_dataEntities())
            {

                //get the details of workout to copy
                workout_custom workout = getWorkout(workoutId);

                //add a new workout and assign to John Doe (11)
                int newWorkoutId = addWorkout(workout.name + " - copy", 11);
                updateWorkout(newWorkoutId, workout.name + " - copy",11, (int)workout.number_of_days, workout.start_date, workout.date_assigned);

                //Update clients current program details
                if (getClientProgramDetails(11, newWorkoutId) == null)
                    addClientProgramDetails(11, (int)workout.number_of_days, newWorkoutId);

                else
                    updateClientProgramDetailsMentor(11, (int)workout.number_of_days, newWorkoutId);


                //get all workout elements/exercises of workout to copy
                for (int i =1; i<workout.number_of_days+1; i++)
                {
                    IEnumerable<workoutelement_custom> elements = WorkoutElement.getWorkoutElementsPerDay(workoutId, i);

                    foreach (workoutelement_custom element in elements) //copy all elements to the new workout
                    {
                        WorkoutElement.addWorkoutElement((int)element.exerciseId, element.repcount, (int)element.sets, element.rest, element.comment, newWorkoutId, i);
                    }
                }

                return getWorkout(newWorkoutId).guid;
            }
        }


        public static bool deleteWorkout(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update workout set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());

                WorkoutFeedback.deleteAllFeedbackForProgram(id);

                return true;
            }
        }

    }
}
