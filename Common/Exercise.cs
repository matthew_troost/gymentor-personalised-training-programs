﻿using Common.Database;
using Common.Database.CustomDataModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Exercise
    {

        public static IEnumerable<exercise> getExercises()
        {
            string sql = @"select *
                            from exercise 
                            where deleted=0
                            order by name";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<exercise> exercises = context.Database.SqlQuery<exercise>(sql).ToList();
                return exercises;
            }
        }

        public static exercise getExercise(int id)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@id", id));

            string sql = @"select *
                            from exercise 
                            where deleted=0
                            and id = @id;";

            using (var context = new gymentor_dataEntities())
            {
                exercise exercise = context.Database.SqlQuery<exercise>(sql,pars.ToArray()).FirstOrDefault();
                return exercise;
            }
        }

        public static exercise getExercise(string guid)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@guid", guid));

            string sql = @"select *
                            from exercise 
                            where deleted=0
                            and guid = @guid;";

            using (var context = new gymentor_dataEntities())
            {
                exercise exercise = context.Database.SqlQuery<exercise>(sql, pars.ToArray()).FirstOrDefault();
                return exercise;
            }
        }

        public static int addExercise(string name, string description)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@Guid", Guid.NewGuid().ToString("N")));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@description", description));

                string sql = @"insert into exercise
                                (guid,name,description)
                                values
                                (@Guid,@name,@description);
                                SELECT LAST_INSERT_ID() as id;";

                int ExerciseId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return ExerciseId;
            }
        }

        public static void updateExercise(int id, string name, string description)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));
                pars.Add(new MySqlParameter("@name", name));
                pars.Add(new MySqlParameter("@description", description));

                string sql = @"update exercise set
                                name = @name,
                                description = @description
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }


        public static bool deleteExercise(int id)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@id", id));

                string sql = @"update exercise set
                                deleted = true
                                where id = @id;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
                return true;
            }
        }

    }
}
