﻿using Common.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class UserStats
    {
        public static user_stats getUserStats(int userId)
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@userId", userId));

            string sql = @"select *
                            from user_stats 
                            where userId = @userId;";

            using (var context = new gymentor_dataEntities())
            {
                user_stats stats = context.Database.SqlQuery<user_stats>(sql, pars.ToArray()).FirstOrDefault();
                return stats;
            }
        }

        public static IEnumerable<user_stats> getUserStatsLastUpdated()
        {
            List<MySqlParameter> pars = new List<MySqlParameter>();
            pars.Add(new MySqlParameter("@last_update", DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd")));

            string sql = @"select * from user_stats where last_updated > @last_update order by last_updated desc;";

            using (var context = new gymentor_dataEntities())
            {
                IEnumerable<user_stats> dates = context.Database.SqlQuery<user_stats>(sql, pars.ToArray()).ToList();
                return dates;
            }
        }

        public static int addUserStats(int userId, int weight, int height, string goal, string injuries, int daysAvailable, string gymHome, 
                                        string history, bool gender, DateTime birthday, string cellnumber)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@userId", userId));
                pars.Add(new MySqlParameter("@weight", weight));
                pars.Add(new MySqlParameter("@height", height));
                pars.Add(new MySqlParameter("@goal", goal));
                pars.Add(new MySqlParameter("@injuries", injuries));
                pars.Add(new MySqlParameter("@daysAvailable", daysAvailable));
                pars.Add(new MySqlParameter("@gymHome", gymHome));
                pars.Add(new MySqlParameter("@history", history));
                pars.Add(new MySqlParameter("@gender", gender));
                pars.Add(new MySqlParameter("@birthday", birthday));
                pars.Add(new MySqlParameter("@cellnumber", cellnumber));

                string sql = @"insert into user_stats
                                (userId,weight,height,goal,injuries,days_training_per_week,gym_home,exercise_history,gender,birthday,cell_number)
                                values
                                (@userId,@weight,@height,@goal,@injuries,@daysAvailable,@gymHome,@history,@gender,@birthday,@cellnumber);
                                SELECT LAST_INSERT_ID() as id;";

                int userStatsId = context.Database.SqlQuery<int>(sql, pars.ToArray()).FirstOrDefault();
                return userStatsId;
            }
        }


        public static void updateFitnessStats(int userId, int weight, int height, string goal, string injuries, int daysAvailable, string gymHome,
                                        string history)
        {
            using (var context = new gymentor_dataEntities())
            {
                List<MySqlParameter> pars = new List<MySqlParameter>();
                pars.Add(new MySqlParameter("@userId", userId));
                pars.Add(new MySqlParameter("@weight", weight));
                pars.Add(new MySqlParameter("@height", height));
                pars.Add(new MySqlParameter("@goal", goal));
                pars.Add(new MySqlParameter("@injuries", injuries));
                pars.Add(new MySqlParameter("@daysAvailable", daysAvailable));
                pars.Add(new MySqlParameter("@gymHome", gymHome));
                pars.Add(new MySqlParameter("@history", history));
                pars.Add(new MySqlParameter("@last_update", DateTime.Parse(DateTime.Today.ToString("yyyy-MM-dd"))));

                string sql = @"update user_stats set
                                weight = @weight,
                                height = @height,
                                goal = @goal,
                                injuries = @injuries,
                                days_training_per_week = @daysAvailable,
                                gym_home = @gymHome,
                                exercise_history = @history,
                                last_updated = @last_update
                                where userId = @userId;";

                context.Database.ExecuteSqlCommand(sql, pars.ToArray());
            }
        }

    }
}
