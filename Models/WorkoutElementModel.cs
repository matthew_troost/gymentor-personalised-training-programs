﻿using Common;
using Common.Database.CustomDataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class WorkoutElementModel
    {
        public int id { get; set; }
        public int Exercise_Id { get; set; }
        public string Exercise_Name { get; set; }
        public string Exercise_Description { get; set; }
        public int FileId { get; set; }
        public string RepCount { get; set; }
        public int Sets { get; set; }
        public string Rest { get; set; }
        public string CommentFromMentor { get; set; }
        public DateTime Date_Created { get; set; }
        public string FileName { get; set; }


        public static IEnumerable<WorkoutElementModel> getAll()
        {
            foreach (workoutelement_custom item in WorkoutElement.getWorkoutElements())
            {
                yield return bindModel(item);
            }
        }

        public static IEnumerable<WorkoutElementModel> getAllPerProgram(int programId, int day)
        {
            foreach (workoutelement_custom item in WorkoutElement.getWorkoutElementsPerDay(programId, day))
            {
                yield return bindModel(item);
            }
        }

        public static WorkoutElementModel bindModel(workoutelement_custom item)
        {
            return new WorkoutElementModel
            {
                id = item.id,
                Exercise_Id = (int)item.exerciseId,
                Exercise_Name = item.exerciseName,
                FileId = 1, ////////////////////////////////////////
                RepCount = item.repcount,
                Sets = (int)item.sets,
                Rest = item.rest,
                CommentFromMentor = item.comment,
                Date_Created = (DateTime)item.date_created,
                Exercise_Description = item.description,
                FileName = item.filename
            };
        }



    }
}
