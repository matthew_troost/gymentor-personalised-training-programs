﻿using Common;
using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class PaymentSuccessModel
    {

        public int OrderNumber { get; set; }
        public string Price { get; set; }

        public static PaymentSuccessModel get(string id)
        {
            order item = Orders.getOrderByOrderID(id);

            return bindModel(item);
        }

        public static PaymentSuccessModel bindModel(order item)
        {
            return new PaymentSuccessModel
            {
                OrderNumber = item.id,
                Price = item.amount
            };
        }

    }
}
