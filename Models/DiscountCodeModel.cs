﻿using Common.Database;
using System;
using Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class DiscountCodeModel
    {
        public int DiscountId { get; set; }
        [Required(ErrorMessage = "Percentage is required")]
        public int Percentage { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public string Code { get; set; }
        public DateTime Date_Created { get; set; }


        public static IEnumerable<DiscountCodeModel> getAll()
        {
            foreach (discount_code item in Payments.getDiscountCodes())
            {
                yield return bindModel(item);
            }
        }

        public static DiscountCodeModel bindModel(discount_code item)
        {
            return new DiscountCodeModel
            {
                DiscountId = item.id,
                Percentage = (int)item.percentage,
                Active = (bool)item.active,
                Deleted = (bool)item.deleted,
                Code = item.code,
                Date_Created = (DateTime)item.date_created
            };
        }


    }
}
