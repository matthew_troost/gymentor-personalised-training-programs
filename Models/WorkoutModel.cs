﻿using Common;
using Common.Database;
using Common.Database.CustomDataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class WorkoutModel
    {
        public int WorkoutId { get; set; }
        public string Guid { get; set; }

        [Required(ErrorMessage = "Workout Name is required")]
        [Display(Name = "Workout Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Client is required")]
        [Display(Name = "Client")]
        public int ClientId { get; set; }
        public DateTime Date_Created { get; set; }

        [Required(ErrorMessage = "Please assign an expiry date")]
        public DateTime Expiry_Date { get; set; }

        [Required(ErrorMessage = "Please assign a start date")]
        public DateTime Start_Date { get; set; }
        public bool Completed { get; set; }
        public bool Saved { get; set; }
        public bool Editing { get; set; }
        public int NumberOfDays { get; set; }
        public string ClientName { get; set; }
        public int CompletedDays { get; set; }


        public static IEnumerable<WorkoutModel> getAll()
        {
            foreach (workout_custom item in Workout.getWorkouts())
            {
                yield return bindModel(item);
            }
        }

        public static WorkoutModel get(string guid)
        {
            return bindModel(Workout.getWorkout(guid));
        }

        public static WorkoutModel get(int id)
        {
            return bindModel(Workout.getWorkout(id));
        }

        public static IEnumerable<WorkoutModel> getForUser(int clientId)
        {
            foreach (workout_custom item in Workout.getWorkoutsForUser(clientId))
            {
                yield return bindModel(item);
            }            
        }

        public static WorkoutModel bindModel(workout_custom item)
        {
            return new WorkoutModel
            {
                WorkoutId = item.id,
                Name = item.name,
                ClientId = (int)item.clientId,
                Date_Created = (DateTime)item.date_created,
                Completed = (bool)item.completed,
                NumberOfDays = (int)item.number_of_days,
                Guid = item.guid,
                Expiry_Date = (DateTime)item.date_assigned,
                Start_Date = (DateTime)item.start_date,
                ClientName = (item.firstname.First().ToString().ToUpper() + item.firstname.Substring(1)) + " " + (item.lastname.First().ToString().ToUpper() + item.lastname.Substring(1)),
                CompletedDays = (item.completed_days == null) ? 0 : (int)item.completed_days
            };
        }


    }
}
