﻿using Common;
using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class WorkoutFeedbackModel
    {
        public DateTime Last_Updated { get; set; }
        public bool Deleted { get; set; }
        public string Feedback { get; set; }
        public int FeedbackId { get; set; }
        public int UserId { get; set; }
        public int ProgramId { get; set; }
        public string UserFullName { get; set; }
        public string ProgramName { get; set; }
        public string ProgramGuid { get; set; }

        public static IEnumerable<WorkoutFeedbackModel> getAll()
        {
            foreach (workout_feedback item in WorkoutFeedback.getFeedback())
            {
                yield return bindModel(item);
            }
        }

        public static IEnumerable<WorkoutFeedbackModel> getLatest()
        {
            foreach (workout_feedback item in WorkoutFeedback.getLatestFeedback())
            {
                yield return bindModel(item);
            }
        }

        public static WorkoutFeedbackModel bindModel(workout_feedback item)
        {
            WorkoutModel relatedWorkout = WorkoutModel.get((int)item.programId);

            return new WorkoutFeedbackModel
            {
                Last_Updated = (DateTime)item.last_updated,
                Deleted = (bool)item.deleted,
                Feedback = item.feedback,
                FeedbackId = (int)item.id,
                UserId = (int)item.userId,
                ProgramId = (int)item.programId,
                UserFullName = UserModel.get((int)item.userId).FullName,
                ProgramName = relatedWorkout.Name,
                ProgramGuid = relatedWorkout.Guid
            };
        }


    }
}
