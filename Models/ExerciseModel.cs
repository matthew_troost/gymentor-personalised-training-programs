﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Database;
using Common;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class ExerciseModel
    {
        public int ExerciseId { get; set; }
        public bool Saved { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }
        public DateTime Date_Created { get; set; }


        public static IEnumerable<ExerciseModel> getAll()
        {
            foreach (exercise item in Exercise.getExercises())
            {
                yield return bindModel(item);
            }
        }

        public static ExerciseModel get(string guid)
        {
            return bindModel(Exercise.getExercise(guid));
        }

        public static ExerciseModel bindModel(exercise item)
        {
            return new ExerciseModel
            {
                ExerciseId = item.id,
                Name = item.name,
                Description = item.description,
                Guid = item.guid,
                Date_Created = (DateTime)item.date_created
            };
        }

    }
}
