﻿using Common;
using Common.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UserModel
    {
        public int id { get; set; }
        public string Guid { get; set; }
        public bool Saved { get; set; }
        public bool Failed { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string SecurityQuestion { get; set; }

        [Required(ErrorMessage = "Required")]
        public string SecurityPassword { get; set; }

        public DateTime DateJoined { get; set; }

        [Display(Name = "Super User")]
        public bool SuperUser { get; set; }


        public static UserModel get(string id)
        {
            user item = User.getUser(id);

            return bindModel(item);
        }

        public static UserModel get(int Userid)
        {
            user item = User.getUser(Userid);

            return bindModel(item);
        }

        public static UserModel getbyUsername(string Username)
        {
            user item = User.getUserbyUsername(Username);

            return bindModel(item);
        }

        public static IEnumerable<UserModel> getAll()
        {
            foreach (user item in User.getUsers())
            {
                yield return bindModel(item);
            }
        }

        public static IEnumerable<UserModel> getAllLegit()
        {
            foreach (user item in User.getLegitUsers())
            {
                yield return bindModel(item);
            }
        }


        public static IEnumerable<UserModel> getAllMentors()
        {
            foreach (user item in User.getMentors())
            {
                yield return bindModel(item);
            }
        }

        public static UserModel bindModel(user item)
        {
            return new UserModel
            {
                id = item.id,
                Guid = item.guid,
                FirstName = item.firstname.First().ToString().ToUpper() + item.firstname.Substring(1),
                LastName = item.lastname.First().ToString().ToUpper() + item.lastname.Substring(1),
                FullName = item.firstname.First().ToString().ToUpper() + item.firstname.Substring(1) + " " + item.lastname.First().ToString().ToUpper() + item.lastname.Substring(1),
                UserName = item.username,
                EmailAddress = item.email,
                Password = item.password,
                DateJoined = (DateTime)item.date_joined,
                SuperUser = (bool)item.superUser,
                SecurityQuestion = item.security_question,
                SecurityPassword = item.security_answer
            };
        }

    }
}
