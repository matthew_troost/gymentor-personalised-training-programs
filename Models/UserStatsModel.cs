﻿using Common;
using Common.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UserStatsModel
    {
        public int StatsId { get; set; }
        public int UserId { get; set; }
        public int MembershipId { get; set; }
        public int ProgramsCompleted { get; set; }
        public int Age { get; set; }

        [Required(ErrorMessage = "Required")]
        public int Weight { get; set; }

        [Required(ErrorMessage = "Required")]
        public int Height { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Goal { get; set; }

        public string Injuries { get; set; }

        [Required(ErrorMessage = "Required")]
        public int Days_Trianing_Per_Week { get; set; }

        public string Gym_Home { get; set; }

        public string ExerciseHistory { get; set; }

        [Required(ErrorMessage = "Required")]
        public DateTime Birthday { get; set; }

        public bool Gender { get; set; }

        public string CellNumber { get; set; }


        public static UserStatsModel get(int UserId)
        {
            return bindModel(UserStats.getUserStats(UserId));
        }

        public static int CalculateAge(DateTime birthday)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (now < birthday.AddYears(age)) age--;
            return age;
        }

        public static UserStatsModel bindModel(user_stats item)
        {
            return new UserStatsModel
            {
                StatsId = item.id,
                UserId = (int)item.userId,
                MembershipId = (int)item.membershipId,
                ProgramsCompleted = (int)item.programsCompleted,
                Age = CalculateAge((DateTime)item.birthday),
                Weight = (int)item.weight,
                Height = (int)item.height,
                Days_Trianing_Per_Week = (int)item.days_training_per_week,
                Goal = item.goal,
                Injuries = item.injuries,
                Gym_Home = item.gym_home,
                ExerciseHistory = item.exercise_history,
                CellNumber = item.cell_number,
                Birthday = (DateTime)item.birthday,
                Gender = (bool)item.gender
            };
        }


    }
}
