﻿using Common;
using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class OrderModel
    {
        public int OrderId { get; set; }
        public int Amount { get; set; }
        public DateTime DateRecieved { get; set; }
        public int UserId { get; set; }
        public string Guid { get; set; }
        public string UserName { get; set; }
        public bool Successfull { get; set; }


        public static IEnumerable<OrderModel> getAll()
        {
            foreach (order item in Orders.getOrders())
            {
                yield return bindModel(item);
            }
        }

        public static IEnumerable<OrderModel> getLatest()
        {
            foreach (order item in Orders.getLatestOrders())
            {
                yield return bindModel(item);
            }
        }

        public static IEnumerable<OrderModel> getFor(int userId)
        {
            foreach (order item in Orders.getOrdersForUser(userId))
            {
                yield return bindModel(item);
            }
        }

        public static OrderModel bindModel(order item)
        {
            return new OrderModel
            {
                OrderId = item.id,
                Amount = int.Parse(item.amount),
                DateRecieved = (DateTime)item.date,
                UserId = (int)item.clientId,
                Guid = item.orderId,
                UserName = UserModel.get((int)item.clientId).FullName,
                Successfull = (bool)item.successfull
            };
        }



    }
}
