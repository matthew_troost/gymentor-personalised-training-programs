﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class RegisterModel
    {

        [Required(ErrorMessage = "Username is required")]
        public string RegisterUsername { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string RegisterPassword { get; set; }

        [Required(ErrorMessage = "Security Question is required")]
        [DataType(DataType.Password)]
        public string SecurityQuestion { get; set; }

        [Required(ErrorMessage = "Security Answer is required")]
        [DataType(DataType.Password)]
        public string SecurityAnswer { get; set; }

        [Required(ErrorMessage = "Firstname is required")]
        public string RegisterFirstName { get; set; }

        [Required(ErrorMessage = "Lastname is required")]
        public string RegisterLastName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public string RegisterEmail { get; set; }

        public bool Failed { get; set; }

    }
}
