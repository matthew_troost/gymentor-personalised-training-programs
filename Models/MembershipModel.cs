﻿using Common;
using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class MembershipModel
    {
        public int MembershipId { get; set; }
        public bool Deleted { get; set; }
        public int NumberOfMonths { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }


        public static IEnumerable<MembershipModel> getAll()
        {
            foreach (membership item in Membership.getMemberships())
            {
                yield return bindModel(item);
            }
        }

        public static MembershipModel get(int id)
        {
            return bindModel(Membership.getMembership(id));
        }

        public static MembershipModel bindModel(membership item)
        {
            return new MembershipModel
            {
                MembershipId = item.id,
                Deleted = (bool)item.deleted,
                NumberOfMonths = (int)item.number_of_months,
                Name = item.name,
                Price = (int)item.price
            };
        }


    }
}
