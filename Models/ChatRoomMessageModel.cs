﻿using Common;
using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ChatRoomMessageModel
    {
        public int id { get; set; }
        public int UserId { get; set; }
        public string Message { get; set; }
        public bool ReadByMentor { get; set; }
        public bool FromMentor { get; set; }
        public DateTime Date_Created { get; set; }


        public static IEnumerable<ChatRoomMessageModel> getMessagesForUser(int UserId)
        {
            foreach (chatroom_messages item in ChatRoomMessage.getMessages(UserId))
            {
                yield return bindModel(item);
            }
        }

        public static ChatRoomMessageModel bindModel(chatroom_messages item)
        {
            return new ChatRoomMessageModel
            {
                id = item.id,
                UserId = (int)item.userId,
                Message = item.message,
                ReadByMentor = (bool)item.readByMentor,
                FromMentor = (bool)item.fromMentor,
                Date_Created = (DateTime)item.date_created
            };
        }




    }
}
