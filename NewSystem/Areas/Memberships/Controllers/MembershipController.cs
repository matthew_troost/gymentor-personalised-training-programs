﻿using Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem.Areas.Memberships.Controllers
{
    public class MembershipController : Controller
    {
        // GET: Memberships/Membership
        public ActionResult List()
        {
            return View();
        }

        public ActionResult GetMemberships([DataSourceRequest] DataSourceRequest request)
        {
            return Json(MembershipModel.getAll().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMembership([DataSourceRequest] DataSourceRequest request, MembershipModel item)
        {
            item.MembershipId = Membership.addMembership(item.Name, item.Price, item.NumberOfMonths);

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateMembership([DataSourceRequest] DataSourceRequest request, MembershipModel item)
        {
            Membership.updateMembership(item.MembershipId, item.Name, item.Price, item.NumberOfMonths);

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteMembership([DataSourceRequest] DataSourceRequest request, MembershipModel item)
        {
            Membership.deleteExercise(item.MembershipId);

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }



    }
}