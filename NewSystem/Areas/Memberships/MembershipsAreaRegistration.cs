﻿using System.Web.Mvc;

namespace NewSystem.Areas.Memberships
{
    public class MembershipsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Memberships";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Memberships_List",
                "Memberships/{action}/{id}",
                new { controller = "Membership", action = "List", id = UrlParameter.Optional }
            );
        }
    }
}