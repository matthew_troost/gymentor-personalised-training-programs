﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem.Areas.Dashboard.Controllers
{
    [SuperUser]

    public class DashboardController : Controller
    {
        // GET: Dashboard/Dashboard
        public ActionResult Analytics()
        {
            return View();
        }

        public ActionResult getOrders([DataSourceRequest] DataSourceRequest request)
        {
            return Json(OrderModel.getLatest().ToDataSourceResult(request));
        }

    }
}