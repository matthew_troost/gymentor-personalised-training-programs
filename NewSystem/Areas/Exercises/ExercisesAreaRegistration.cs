﻿using System.Web.Mvc;

namespace NewSystem.Areas.Exercises
{
    public class ExercisesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Exercises";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Exercises_List",
                "Exercises/{action}/{id}",
                new { controller = "Exercise", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}