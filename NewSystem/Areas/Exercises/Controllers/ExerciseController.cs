﻿using Models;
using Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem.Areas.Exercises.Controllers
{
    [SuperUser]

    public class ExerciseController : Controller
    {
        // GET: Exercises/Exercise
        public ActionResult List()
        {
            return View();
        }

        public ActionResult logintest()
        {
            return View();
        }

        public ActionResult Single(string id)
        {
            ExerciseModel model = new ExerciseModel();

            if (string.IsNullOrEmpty(id))
                model.ExerciseId = 0;
            else
                model = ExerciseModel.get(id);

            if (model == null)
                return RedirectToAction("List");

            return View("Single", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Single(ExerciseModel model, IEnumerable<HttpPostedFileBase> files)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please fill in the required fields");
                return View(model);
            }

            if (model.ExerciseId == 0)
            {
                model.ExerciseId = Exercise.addExercise(model.Name, model.Description);
                model.Saved = true;
            }
            else
                Exercise.updateExercise(model.ExerciseId, model.Name, model.Description);


            if (files != null)
            {
                foreach (HttpPostedFileBase file in files)
                {

                    // Specify the path to save the uploaded file to.
                    string savePath = Server.MapPath("~/Content/videos/");

                    // Get the name of the file to upload.
                    string fileName = file.FileName;

                    // Create the path and file name to check for duplicates.
                    string pathToCheck = savePath + fileName;

                    // Create a temporary file name to use for checking duplicates.
                    string tempfileName = "";

                    // Check to see if a file already exists with the same name as the file to upload.        
                    if (System.IO.File.Exists(pathToCheck))
                    {
                        int counter = 2;
                        while (System.IO.File.Exists(pathToCheck))
                        {
                            // if a file with this name already exists, prefix the filename with a number.
                            tempfileName = "(" + counter.ToString() + ")" + fileName;
                            pathToCheck = savePath + tempfileName;
                            counter++;
                        }

                        fileName = tempfileName;

                    }

                    // Append the name of the file to upload to the path.
                    savePath += fileName;

                    // Call the SaveAs method to save the uploaded file to the specified directory.
                    file.SaveAs(savePath);

                    if (model.ExerciseId == 0)
                        Files.addFile(model.ExerciseId, fileName, file.ContentLength, file.ContentType);
                    else
                        Files.updateFile(model.ExerciseId, fileName, file.ContentLength, file.ContentType);

                }
            }

            ModelState.Clear();

            return RedirectToAction("List");
        }


        public ActionResult getExercises([DataSourceRequest] DataSourceRequest request)
        {
            return Json(ExerciseModel.getAll().ToDataSourceResult(request));
        }

        public JsonResult DeleteExercise(int id)
        {
            return Json(Exercise.deleteExercise(id), JsonRequestBehavior.AllowGet);
        }

    }
}