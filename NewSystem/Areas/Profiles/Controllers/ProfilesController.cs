﻿using Common;
using Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace NewSystem.Areas.Profiles.Controllers
{
    public class ProfilesController : Controller
    {
        // GET: Profiles/Profiles
        public ActionResult MyProfile()
        {
            UserStatsModel model = UserStatsModel.get(int.Parse(Models.ApplicationUser.getPropertyValue("UserID")));

            if (model == null)
                return RedirectToAction("Login", "Authentication", new { area = "Authentication" });

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MyProfile(UserStatsModel model, IEnumerable<HttpPostedFileBase> files)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please fill in the required fields");
                return View(model);
            }

            int UserId = int.Parse(Models.ApplicationUser.getPropertyValue("UserID"));

            if (files != null)
            {
                foreach (HttpPostedFileBase file in files)
                {

                    // Specify the path to save the uploaded file to.
                    string savePath = Server.MapPath("~/Content/images/profilepics/");

                    // Get the name of the file to upload.
                    string fileName = file.FileName;

                    // Create the path and file name to check for duplicates.
                    string pathToCheck = savePath + fileName;

                    // Create a temporary file name to use for checking duplicates.
                    string tempfileName = "";

                    // Check to see if a file already exists with the same name as the file to upload.        
                    if (System.IO.File.Exists(pathToCheck))
                    {
                        int counter = 2;
                        while (System.IO.File.Exists(pathToCheck))
                        {
                            // if a file with this name already exists, prefix the filename with a number.
                            tempfileName = counter.ToString() + fileName;
                            pathToCheck = savePath + tempfileName;
                            counter++;
                        }

                        fileName = tempfileName;

                    }

                    // Append the name of the file to upload to the path.
                    savePath += fileName;

                    WebImage img = new WebImage(file.InputStream);
                    img.Resize(180, 180);

                    // Call the SaveAs method to save the uploaded file to the specified directory.
                    img.Save(savePath);

                    if (Files.geProfilePicture(UserId) != null)
                    {
                        // Update file details
                        Files.updateProfilePic(UserId, fileName, file.ContentLength, file.ContentType);
                    }
                    else { 
                        // Add file details to database
                        Files.addProfilePic(UserId, fileName, file.ContentLength, file.ContentType);
                    }
                }
            }

            UserStats.updateFitnessStats(UserId, model.Weight, model.Height, model.Goal, model.Injuries, model.Days_Trianing_Per_Week, model.Gym_Home ,model.ExerciseHistory);

            ModelState.Clear();

            return View(model);
        }

    }
}