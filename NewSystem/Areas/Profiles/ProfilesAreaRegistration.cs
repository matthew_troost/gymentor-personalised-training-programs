﻿using System.Web.Mvc;

namespace NewSystem.Areas.Profiles
{
    public class ProfilesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Profiles";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Profiles_MyProfile",
                "Profiles/{action}/{id}",
                new { controller = "Profiles", action = "MyProfile", id = UrlParameter.Optional }
            );
        }

    }
}