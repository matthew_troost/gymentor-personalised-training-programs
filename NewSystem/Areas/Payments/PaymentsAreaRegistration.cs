﻿using System.Web.Mvc;

namespace NewSystem.Areas.Payments
{
    public class PaymentsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Payments";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Payments_default",
                "Payments/{action}/{id}",
                new { controller = "Payments", action = "Pay", id = UrlParameter.Optional }
            );
        }
    }
}