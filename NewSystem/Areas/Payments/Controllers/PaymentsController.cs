﻿using Common.Database;
using Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewSystem.Models;
using Common;
using System.Collections.Specialized;
using static System.Net.WebRequestMethods;
using System.Collections;
using System.Net;
using System.Text;

namespace NewSystem.Areas.Payments.Controllers
{
    public class PaymentsController : Controller
    {
        string orderId = "";
        string processorOrderId = "";
        string strPostedVariables = "";

        public ActionResult Pay()
        {
            return View();
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Manage(string id)
        {
            return View();
        }


        public ActionResult PaymentSuccess(string id)
        {
            PaymentSuccessModel model = new PaymentSuccessModel();

            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Pay");
            }
            else
                model = PaymentSuccessModel.get(id);

            if (model == null)
                return RedirectToAction("List");


            Orders.orderSuccess(id);
            return View(model);
            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(DiscountCodeModel model)
        {
            Common.Payments.addDiscountCode(model.Percentage);

            ModelState.Clear();

            return View();
        }

        public ActionResult Checkout(string id)
        {
            PaymentRequestModel model = new PaymentRequestModel();

            if (!string.IsNullOrEmpty(id))
                model.RequestId = id;
            else
                return RedirectToAction("Pay");

            return View(model);
        }

        public JsonResult GetMembershipDetails(int id)
        {
            return Json(MembershipModel.get(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRedirectString(int amount, int userId)
        {
            return Json(Common.Payments.btnOrder_Click(amount.ToString(), userId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult getDiscountCodes([DataSourceRequest] DataSourceRequest request)
        {
            return Json(DiscountCodeModel.getAll().ToDataSourceResult(request));
        }

        public JsonResult DeleteDiscountCode(int id)
        {
            return Json(Common.Payments.deleteDiscountCode(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActivateDiscountCode(int id)
        {
            return Json(Common.Payments.activateDiscountCode(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeactivateDiscountCode(int id)
        {
            return Json(Common.Payments.deactivateDiscountCode(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ConfirmDiscountCode(string code)
        {
            discount_code dc = Common.Payments.confirmDiscountCode(code);
            if(dc == null)
                return Json(false, JsonRequestBehavior.AllowGet);
            else
                return Json(dc, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getOrders([DataSourceRequest] DataSourceRequest request)
        {
            return Json(OrderModel.getAll().ToDataSourceResult(request));
        }

        public ActionResult getOrdersForUser([DataSourceRequest] DataSourceRequest request)
        {
            return Json(OrderModel.getFor(int.Parse(ApplicationUser.getPropertyValue("UserID"))).ToDataSourceResult(request));
        }

    }
}