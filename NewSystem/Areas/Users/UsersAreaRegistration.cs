﻿using System.Web.Mvc;

namespace NewSystem.Areas.Clients
{
    public class UsersAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Users";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Users_List",
                "Users/{action}/{id}",
                new { controller = "Users", action = "List", id = UrlParameter.Optional }
            );
        }
    }
}