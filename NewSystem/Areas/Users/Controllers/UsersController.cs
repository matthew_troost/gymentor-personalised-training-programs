﻿using Common.Database;
using Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem.Areas.Clients.Controllers
{
    [SuperUser]

    public class UsersController : Controller
    {
        [AllowAnonymous]
        public ActionResult List()
        {
            return View();
        }

        public ActionResult Mentors()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Mentors(UserModel model)
        {
            if (model.FirstName == null || model.LastName == null || model.UserName == null || model.EmailAddress == null)
            {
                ModelState.AddModelError("", "Please fill in the required fields");
                return View(model);
            }

            user UserAvailable = Common.User.CheckUserNameAvailability(model.UserName, null, null);
            if (UserAvailable != null)
            {
                model.Failed = true;
                ModelState.Clear();
                return View(model);
            }

            Common.User.addMentor(model.FirstName, model.LastName, model.EmailAddress, model.UserName);
            model.Saved = true;

            ModelState.Clear();

            //clear fields
            model.FirstName = null;
            model.LastName = null;
            model.EmailAddress = null;
            model.UserName = null;

            return View(model);
        }


        public ActionResult Single(string id)
        {
            UserModel model = new UserModel();

            if (string.IsNullOrEmpty(id))
            {
                model.id = 0;
            }
            else
                model = UserModel.get(id);

            if (model == null)
                return RedirectToAction("List");

            return View(model);
        }

        public ActionResult getMentors([DataSourceRequest] DataSourceRequest request)
        {
            return Json(UserModel.getAllMentors().ToDataSourceResult(request));
        }


    }
}