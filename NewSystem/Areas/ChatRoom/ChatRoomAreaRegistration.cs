﻿using System.Web.Mvc;

namespace NewSystem.Areas.ChatRoom
{
    public class ChatRoomAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ChatRoom";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ChatRoom_Chat",
                "ChatRoom/{action}/{id}",
                new { controller = "ChatRoom", action = "Chat", id = UrlParameter.Optional }
            );
        }
    }
}