﻿using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem.Areas.ChatRoom.Controllers
{
    public class ChatRoomController : Controller
    {
        // GET: ChatRoom/ChatRoom
        public ActionResult Chat()
        {
            return View();
        }

        public JsonResult getMessages()
        {
            user User = Common.User.getUser(int.Parse(Models.ApplicationUser.getPropertyValue("UserID")));
            IEnumerable<chatroom_messages> messages = Common.ChatRoomMessage.getMessages(User.id);
            string html = "";
            int counter = messages.Count();

            foreach (chatroom_messages message in messages)
            {
                if (message.fromMentor == true)
                    html += $"<span class='label label-success'>Mentor - {(message.date_created).ToString("dd MMM yyyy - HH:mm")}</span>";
                else
                    html += $"<span class='label label-info'>You - {message.date_created.ToString("dd MMM yyyy - HH:mm")}</span>";

                if (counter == 1)
                    html += "<p>" + message.message + "</p>";
                else
                    html += "<p>" + message.message + "</p><hr>";

                counter--;
            }

            return Json(html, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReplyToMessage(string html)
        {
            return Json(Common.ChatRoomMessage.sendMessage(int.Parse(Models.ApplicationUser.getPropertyValue("UserID")), html, false, false), JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateUserInfo()
        {
            return Json(Common.ChatRoomMessage.updateUnreadByUser(int.Parse(Models.ApplicationUser.getPropertyValue("UserID"))), JsonRequestBehavior.AllowGet);
        }

    }
}