﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;
using Common.Database;
using Common;
using NewSystem.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Security.Principal;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Security;
using Common.Database.CustomDataModels;

namespace NewSystem.Areas.Authentication.Controllers
{
    public class AuthenticationController : Controller
    {
        private IAuthenticationManager AuthenticationManager { get { return HttpContext.GetOwinContext().Authentication; } }

        // GET: Authentication/Authentication
        [AllowAnonymous]
        public ActionResult Login(string recentlySignedUp)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (bool.Parse(ApplicationUser.getPropertyValue("SuperUser")) == true)
                    return RedirectToAction("Analytics", "Dashboard", new { area = "Dashboard" });
                else
                    return RedirectToAction("MyProfile", "Profiles", new { area = "Profiles" });
            }

            LoginModel model = new LoginModel();

            if (string.IsNullOrEmpty(recentlySignedUp))
                return View(model);
            else
            {
                model.JustSignedUp = true;
                return View(model);
            }
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please enter your credentials");
                return View(model);
            }

            user user = Common.User.login(model.LoginUsername, model.LoginPassword);

            if (user == null)
            {
                //return that it failed
                model.Failed = true;
                return View(model);
            }

            if (UserStats.getUserStats(user.id) == null)
            {
                //return questionaire/stats screen
                return RedirectToAction("Survey", new { id = user.guid });
            }

            //Log them in
            //create the custom application user
            var userClaim = new ApplicationUser()
            {
                Id = user.id.ToString(),
                UserName = user.username,
                Guid = user.guid,
                UserID = user.id,
                FirstName = user.firstname,
                LastName = user.lastname,
                FullName = $"{user.firstname} {user.lastname}",
                Email = user.email,
                SuperUser = user.superUser == true,
                Password = user.password,
                SecurityStamp = Guid.NewGuid().ToString(),
            };

            //CustomUserManager userManager = HttpContext.GetOwinContext().GetUserManager<CustomUserManager>();
            CustomUserManager userManager = new CustomUserManager();

            //create the user identity which will be stored in the httpcontext.current.user property going forward
            ClaimsIdentity identity = userManager.CreateIdentity(userClaim, DefaultAuthenticationTypes.ApplicationCookie);

            //add each custom property as a claim so we can access it once the user is logged in (without pulling it from the db)
            identity.AddClaims(ApplicationUser.populateClaims(userClaim));

            AuthenticationProperties authProperties = new AuthenticationProperties();
            authProperties.ExpiresUtc = DateTime.UtcNow.AddDays(!model.Loginrememberme ? 3 : 45);

            //sign the user in
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(authProperties, identity);

            //FormsAuthentication.SetAuthCookie(ApplicationUser.getPropertyValue("id").ToString(), false);

            userManager.Dispose();

            //update the last seen for this user
            Common.User.updateLastSeen(user.id);

            if (user.superUser == true) 
                return RedirectToAction("Analytics", "Dashboard", new { area = "Dashboard" });
            else
                return RedirectToAction("MyProfile", "Profiles", new { area = "Profiles" });

        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            RegisterModel model = new RegisterModel();

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please enter your credentials");
                return View(model);
            }

            user UserAvailable = Common.User.CheckUserNameAvailability(model.RegisterUsername, model.RegisterFirstName, model.RegisterLastName);
            if(UserAvailable != null)
            {
                model.Failed = true;
                return View(model);
            }

            int userId = Common.User.addUser(model.RegisterFirstName, model.RegisterLastName, model.RegisterEmail, model.RegisterPassword, model.RegisterUsername, model.SecurityQuestion, model.SecurityAnswer);

            string userguid = UserModel.get(userId).Guid;

            return RedirectToAction("Survey", "Authentication", new { id = userguid });
        }


        [AllowAnonymous]
        public ActionResult Survey(string id)
        {
            UserStatsModel model = new UserStatsModel();

            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Register");
            else
            {
                UserModel user = UserModel.get(id);

                if (user == null)
                    return RedirectToAction("Register");

                model.UserId = user.id;
            }

            return View("Survey", model);
        }

        [AllowAnonymous]
        public ActionResult LostPassword(string id)
        {
            UserModel model = new UserModel();

            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Register");
            else
            {
                model = UserModel.get(id);

                if (model == null)
                    return RedirectToAction("Register");
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Survey(UserStatsModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please enter your credentials");
                return View(model);
            }

            if (model.Gym_Home == null)
                model.Gym_Home = "Home";

            model.Age = UserStatsModel.CalculateAge(model.Birthday);

            UserStats.addUserStats(model.UserId,model.Weight,model.Height,model.Goal,model.Injuries,model.Days_Trianing_Per_Week,model.Gym_Home,
                                    model.ExerciseHistory,model.Gender,model.Birthday,model.CellNumber);

            LoginModel logmod = new LoginModel();
            logmod.JustSignedUp = true;

            return RedirectToAction("Login", "Authentication", new { recentlySignedUp = "true" });
        }

        [AllowAnonymous]
        public JsonResult GetUserDetails(string username)
        {
            return Json(Common.User.getUserbyUsername(username), JsonRequestBehavior.AllowGet);
        }



    }
}