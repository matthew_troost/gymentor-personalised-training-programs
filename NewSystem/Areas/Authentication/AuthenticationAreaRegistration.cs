﻿using System.Web.Mvc;

namespace NewSystem.Areas.Authentication
{
    public class AuthenticationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Authentication";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Authentication_default",
                "Authentication/{action}/{id}",
                new { controller = "Authentication", action = "Login", id = UrlParameter.Optional }
            );

            //context.MapRoute(
            //    "Authentication_Survey_New",
            //    "Invoices/{action}/{id}/{UserID}",
            //    new { controller = "Authentication", action = "Survey", id = UrlParameter.Optional, UserID = UrlParameter.Optional }
            //);

        }
    }
}