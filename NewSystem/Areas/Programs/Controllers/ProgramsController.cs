﻿using Common;
using Common.Database;
using Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem.Areas.Programs.Controllers
{
    public class ProgramsController : Controller
    {
        // GET: Programs/Programs
        [SuperUser]
        public ActionResult CreateNew(string id)
        {
            WorkoutModel model = new WorkoutModel();

            if (string.IsNullOrEmpty(id))
                model.WorkoutId = 0;
            else
            {
                model = WorkoutModel.get(id);
                model.Editing = true;
            }

            if (model == null)
                return RedirectToAction("List");

            return View(model);
        }

        [SuperUser]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNew(WorkoutModel model)
        {

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please fill in the required fields");
                return View(model);
            }

            if (model.WorkoutId == 0)
            {
                //Add workout
                model.WorkoutId = Workout.addWorkout(model.Name, model.ClientId);
                model.Saved = true;

                ModelState.Clear();

                return View(model);
            }
            else
            {
                //Update workout        
                Workout.updateWorkout(model.WorkoutId, model.Name, model.ClientId, model.NumberOfDays, model.Start_Date, model.Expiry_Date);

                //delete workout details of previous user if changing the user
                if (Workout.getSpecificAllClientProgramDetails(model.ClientId, model.WorkoutId) != 0)
                    Workout.deleteClientProgramDetails(model.WorkoutId);

                //Update clients current program details
                if (Workout.getClientProgramDetails(model.ClientId, model.WorkoutId) == null)
                    Workout.addClientProgramDetails(model.ClientId, model.NumberOfDays, model.WorkoutId);
                
                else
                    Workout.updateClientProgramDetailsMentor(model.ClientId, model.NumberOfDays, model.WorkoutId);

                ModelState.Clear();

                return RedirectToAction("List");
            }


        }

        public ActionResult MyPrograms()
        {
            return View();
        }

        public ActionResult Feedback()
        {
            return View();
        }

        [SuperUser]
        public ActionResult List()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Landing()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Test()
        {
            return View();
        }


        public ActionResult getWorkouts([DataSourceRequest] DataSourceRequest request)
        {
            return Json(WorkoutModel.getAll().ToDataSourceResult(request));
        }

        public ActionResult GetWorkoutElementsPerDay([DataSourceRequest] DataSourceRequest request, int programId, int day)
        {
            return Json(WorkoutElementModel.getAllPerProgram(programId, day).ToDataSourceResult(request));
        }

        public JsonResult DeleteWorkout(int id)
        {
            return Json(Workout.deleteWorkout(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateClientCurrentWorkoutDetails(int completedDays, string checkedDays, int programId, string feedback)
        {
            int userId = int.Parse(Models.ApplicationUser.getPropertyValue("UserID"));

            if (feedback != "")
            {
                workout_feedback feed = WorkoutFeedback.getFeedback(userId, programId);
                //check if feedback has been made before
                if (feed != null) { 
                    if (!feed.feedback.Equals(feedback))
                    {
                        WorkoutFeedback.updateFeedback(feed.id, feedback);
                    }
                }
            //else add new
            else
                WorkoutFeedback.addFeedback(feedback, userId, programId);
            }

            return Json(Workout.updateClientProgramDetailsUser(userId, completedDays, checkedDays, programId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClientProgramDetails()
        {
            return Json(Workout.getAllClientProgramDetails(int.Parse(Models.ApplicationUser.getPropertyValue("UserID"))), JsonRequestBehavior.AllowGet);
        }

        public JsonResult copyWorkout(int workoutId)
        {
            return Json(Workout.copyWorkout(workoutId), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddWorkoutElement([DataSourceRequest] DataSourceRequest request, WorkoutElementModel item, int programId, int day)
        {
            if (item != null && ModelState.IsValid)
                item.id = WorkoutElement.addWorkoutElement(item.Exercise_Id, item.RepCount, item.Sets, item.Rest, item.CommentFromMentor, programId, day);

            exercise ex = Exercise.getExercise(item.Exercise_Id);
            item.Exercise_Name = ex.name;

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateWorkoutElement([DataSourceRequest] DataSourceRequest request, WorkoutElementModel item)
        {
            if (item != null && ModelState.IsValid)
                WorkoutElement.updateWorkoutElement(item.id, item.Exercise_Id, item.RepCount, item.Sets, item.Rest, item.CommentFromMentor);

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWorkoutElement([DataSourceRequest] DataSourceRequest request, WorkoutElementModel item)
        {
            if (item != null && ModelState.IsValid)
                WorkoutElement.deleteWorkoutElement(item.id);

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

    }
}