﻿using System.Web.Mvc;

namespace NewSystem.Areas.Programs
{
    public class ProgramsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Programs";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Programs_CreateNew",
                "Programs/{action}/{id}",
                new { controller = "Programs", action = "CreateNew", id = UrlParameter.Optional }
            );
        }
    }
}