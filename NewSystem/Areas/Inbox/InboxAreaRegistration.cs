﻿using System.Web.Mvc;

namespace NewSystem.Areas.Inbox
{
    public class InboxAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Inbox";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Inbox_List",
                "Inbox/{action}/{id}",
                new { controller = "Inbox", action = "List", id = UrlParameter.Optional }
            );
        }
    }
}