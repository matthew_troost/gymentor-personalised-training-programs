﻿using Common.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem.Areas.Inbox.Controllers
{
    [SuperUser]

    public class InboxController : Controller
    {
        // GET: Inbox/Inbox
        public ActionResult List()
        {
            return View();
        }

        public JsonResult getMessages(int userId)
        {

            IEnumerable<chatroom_messages> messages = Common.ChatRoomMessage.getMessages(userId);
            user User = Common.User.getUser(userId);
            string html = "";
            int counter = messages.Count();

            foreach (chatroom_messages message in messages)
            {
                if (message.fromMentor == true)
                    html += $"<span class='label label-success'>Mentor - {(message.date_created).ToString("dd MMM yyyy - HH:mm")}</span>";
                else
                    html += $"<span class='label label-info'>{User.firstname} - {message.date_created.ToString("dd MMM yyyy - HH:mm")}</span>";

                if (counter == 1) 
                    html += "<p>" + message.message + "</p>";
                else
                    html += "<p>" + message.message + "</p><hr>";

                counter--;
            }

            return Json(html, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReplyToMessage(int userId, string html)
        {
            return Json(Common.ChatRoomMessage.sendMessage(userId,html,true,true), JsonRequestBehavior.AllowGet);
        }


    }
}