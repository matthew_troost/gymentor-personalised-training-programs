﻿using System.Web.Mvc;

namespace NewSystem.Areas.Error
{
    public class ErrorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Error";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Error_default",
                "Error/{action}/{id}",
                new { controller = "Error", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}