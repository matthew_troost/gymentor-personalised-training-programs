/// <autosync enabled="true" />
/// <reference path="addtohomescreen.min.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="bootstrap-progressbar.min.js" />
/// <reference path="custom.min.js" />
/// <reference path="daterangepicker.js" />
/// <reference path="gauge.min.js" />
/// <reference path="icheck.min.js" />
/// <reference path="index.js" />
/// <reference path="jquery.easypiechart.min.js" />
/// <reference path="jquery.min.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-3.1.1.js" />
/// <reference path="jquery-3.1.1.min.js" />
/// <reference path="jquery-3.1.1.slim.min.js" />
/// <reference path="kendo.all.min.js" />
/// <reference path="kendo.aspnetmvc.min.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="moment.min.js" />
/// <reference path="pnotify.custom.min.js" />
/// <reference path="respond.matchmedia.addlistener.min.js" />
/// <reference path="respond.min.js" />
