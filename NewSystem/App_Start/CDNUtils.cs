﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewSystem
{
    public static class CDNUtils
    {
        public static string CdnUrl(this HtmlHelper helper, string contentPath)
        {
            // Check if we are in release mode. If not, then simply return as normal
#if (!DEBUG)

    // The content path will often get passed in with a leading ‘~’ sign. We need to remove it // if we append the URL of the CDN to it.
    if (contentPath.StartsWith("~"))					
    {
        contentPath = contentPath.Substring(1);
    }

    // Retrieve the URL of the CDN from the Web.config
    string appSetting = ConfigurationManager.AppSettings["CDNUrl"];	

    // Combine the two URLs and update the content
    Uri combinedUri = new Uri(new Uri(appSetting), contentPath);
    contentPath = combinedUri.ToString();					
            
#endif

            // Create the correct URL
            var url = new UrlHelper(helper.ViewContext.RequestContext);

            // Return the new and updated content path
            return url.Content(contentPath);
        }


    }
}